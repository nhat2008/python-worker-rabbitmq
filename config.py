# -*- encoding: utf-8 -*-
import os


def get_config():
    config = DevelopmentConfig()

    # If's for running by using docker
    if os.environ.get('RBMQ_CONNECTION', False):
        config.RBMQ_CONNECTION = os.environ['RBMQ_CONNECTION']
    if os.environ.get('RBMQ_PORT', False):
        config.RBMQ_PORT = os.environ['RBMQ_PORT']
    if os.environ.get('RBMQ_USER', False):
        config.RBMQ_USER = os.environ['RBMQ_USER']
    if os.environ.get('RBMQ_PASS', False):
        config.RBMQ_PASS = os.environ['RBMQ_PASS']
    if os.environ.get('RBMQ_QUEUE', False):
        config.RBMQ_QUEUE = os.environ['RBMQ_QUEUE']
    if os.environ.get('RBMQ_EXCHANGE', False):
        config.RBMQ_EXCHANGE = os.environ['RBMQ_EXCHANGE']
    if os.environ.get('RBMQ_ROUTE', False):
        config.RBMQ_ROUTE = os.environ['RBMQ_ROUTE']

    if os.environ.get('LOG_FILE', False):
        config.LOG_FILE = os.environ['LOG_FILE']
    return config

class DevelopmentConfig():
    RBMQ_CONNECTION = '172.17.0.5'
    RBMQ_USER = ''
    RBMQ_PASS = ''
    RBMQ_PORT = 5672
    RBMQ_QUEUE = ''
    RBMQ_EXCHANGE = ''
    RBMQ_ROUTE = ''
    LOG_FILE = '/var/log/supporting-worker.log'