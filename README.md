#### WORKER for RabbitMQ

1. Install python 2.7
2. Install RabbitMQ Server [Notes: should install "RabbitMQ Management" for monitoring]
3. Install dependencies by command:
    "pip install -r requirements.txt"
4. Config in config.cfg
    RBMQ_CONNECTION : connection to RBMQ server
    RBMQ_USER : user can login and use queue in RBMQ server
    RBMQ_PASS : password for user
    RBMQ_PORT : listening port for RBMQ server
    RBMQ_QUEUE : listening queue which worker will work on

5. Run Worker by command:
    "python run.py"
