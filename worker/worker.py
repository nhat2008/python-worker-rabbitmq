#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pika
import json
import task as task
import logging
import os
import config

app_config = config.get_config()

log_file = app_config.LOG_FILE
logging.basicConfig(filename=log_file, level=logging.INFO)


class Worker(object):
    def __init__(self, host='localhost', port=5672, user=None, passwd=None, queue=None):
        logging.info("[x] initializing Configs for Worker")
        try:
            # Config RBMQ to run
            self.credentials = pika.PlainCredentials(user, passwd)
            self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=host, port=port, credentials=self.credentials))
            self.channel  = self.connection.channel()
            self.queue = queue
        except Exception as e:
            logging.error("[x] Error when initializing Worker")
            logging.error(e)

    def init_queue(self, exchange_name, key_route, queue_name):
        logging.info("[x] initializing Configs for Queue")
        try:
            self.channel.queue_declare(queue=queue_name, durable=True)
            self.channel.exchange_declare(exchange=exchange_name, exchange_type='fanout')
            self.channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key=key_route)
        except Exception as e:
            logging.error("[x] Error when Configings for worker")
            logging.error(e)
            self.channel = self.connection.channel()

    def callback(self, ch, method, properties, body):
        logging.info("[x] Received %r" % (body,))
        try:
            data = json.loads(body)
            # Push your task in here
        except Exception as e:
            logging.error("[x] Error when calling API")
            logging.error(body)
            logging.error(e)

        # task.call_api.call_api_test()
        ch.basic_ack(delivery_tag = method.delivery_tag)
        logging.info("[x] Done")

    def run_worker(self):
        logging.info("[x] Running Worker")
        try:
            self.channel.basic_qos(prefetch_count=1)
            self.channel.basic_consume(self.callback, queue=self.queue)
            self.channel.start_consuming()
        except Exception as e:
            logging.error("[x] Error when Running Worker")
            logging.error(e)
