#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json
import logging
import hashlib
import datetime
import os
import config

# Start: Init Config
app_config = config.get_config()
# End: Init Config

log_file = app_config.LOG_FILE
logging.basicConfig(filename=log_file, level=logging.INFO)

def your_task():
    pass