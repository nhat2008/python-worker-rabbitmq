#!/usr/bin/env python
# -*- coding: utf-8 -*-

from worker.worker import Worker as Worker
import config

if __name__ == '__main__':
    # Tool config
    app_config = config.get_config()

    # RabbitMQ config
    rbmq_host = app_config.RBMQ_CONNECTION
    rbmq_port = app_config.RBMQ_PORT
    rbmq_user = app_config.RBMQ_USER
    rbmq_passwd = app_config.RBMQ_PASS
    rbmq_queue = app_config.RBMQ_QUEUE
    rbmq_exchange = app_config.RBMQ_EXCHANGE
    rbmq_route_key = app_config.RBMQ_ROUTE

    # Run new_worker
    new_worker = Worker(host=rbmq_host, port=int(rbmq_port), user=rbmq_user, passwd=rbmq_passwd, queue=rbmq_queue)
    new_worker.init_queue(rbmq_exchange, rbmq_route_key, rbmq_queue)
    new_worker.run_worker()
